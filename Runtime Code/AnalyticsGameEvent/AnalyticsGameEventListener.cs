using com.FDT.GameEvents;
using UnityEngine.Events;

namespace com.FDT.Analytics
{
	public class AnalyticsGameEventListener : GameEvent1Listener<AnalyticEventAsset, AnalyticsGameEvent, AnalyticsGameEventListener.UEvt>
	{
		[System.Serializable]
		public class UEvt : UnityEvent<AnalyticEventAsset>
		{}
	}
}
