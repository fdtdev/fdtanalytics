using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.Analytics
{
	[CreateAssetMenu(menuName = "FDT/FirebaseUtils/FirebaseAnalyticsGameEvent")]
	public class AnalyticsGameEvent : GameEvent1<AnalyticEventAsset>
	{
		public override string arg0label { get { return "eventAsset"; } }
	}
}
