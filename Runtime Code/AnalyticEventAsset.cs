﻿using System.Collections.Generic;
using com.FDT.Common;
using UnityEngine;

namespace com.FDT.Analytics
{
    [CreateAssetMenu(menuName = "FDT/FirebaseUtils/AnalyticEventAsset")]
    public class AnalyticEventAsset : IDAsset
    {
        protected virtual void Reset()
        {
            analyticsEventName = name;
        }

        public string analyticsEventName;
        public List<AnalyticsVar> parameters = new List<AnalyticsVar>();

        public Dictionary<string, object> UpdateData()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            for (int i = 0; i < parameters.Count; i++)
            {
                var parameter = parameters[i];
                object r = null;
                switch (parameter.varType)
                {
                    case AnalyticsVar.AnalyticsVarType.INT:
                    case AnalyticsVar.AnalyticsVarType.LONG:
                        r = parameter.intCallback.Invoke();
                        break;
                    case AnalyticsVar.AnalyticsVarType.BOOL:
                        r = parameter.boolCallback.Invoke();
                        break;
                    case AnalyticsVar.AnalyticsVarType.FLOAT:
                    case AnalyticsVar.AnalyticsVarType.DOUBLE:
                        r = parameter.floatCallback.Invoke();
                        break;
                    case AnalyticsVar.AnalyticsVarType.STRING:
                        r = parameter.stringCallback.Invoke();
                        break;

                }
                result.Add(parameter.varName, r);
            }

            return result;
        }

        public AnalyticsVar GetAnalyticsVarByName(string pKey)
        {
            foreach (var p in parameters)
            {
                if (p.varName == pKey)
                {
                    return p;
                }
            }

            return null;
        }
    }
}