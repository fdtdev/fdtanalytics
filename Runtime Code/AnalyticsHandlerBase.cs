﻿using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.Analytics
{
    public abstract class AnalyticsHandlerBase : MonoBehaviour
    {
        public AnalyticsGameEventHandle _analyticsEvt;
        private void OnEnable()
        {
            _analyticsEvt.AddEventListener(HandleAnalyticsEvent);
        }

        private void HandleAnalyticsEvent(AnalyticEventAsset obj)
        {
            if (obj == null)
            {
                Debug.LogError("Analytics event sended without asset.");
            }
            Dictionary<string, object> result = obj.UpdateData();
            SendAnalytics(obj, result);
        }

        protected abstract void SendAnalytics(AnalyticEventAsset obj, Dictionary<string, object> parameters);

        private void OnDisable()
        {
            _analyticsEvt.RemoveEventListener(HandleAnalyticsEvent);
        }
    }
}