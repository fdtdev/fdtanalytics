﻿using System;

namespace com.FDT.Analytics
{
    [System.Serializable]
    public class AnalyticsVar
    {
        public string varName;
        public AnalyticsVarType varType;
        public BoolCallback boolCallback;
        public IntCallback intCallback;
        public StringCallback stringCallback;
        public FloatCallback floatCallback;

        public enum AnalyticsVarType
        {
            NOT_SET = 0,
            BOOL = 1,
            INT = 2,
            STRING = 3,
            FLOAT = 4,
            LONG = 5,
            DOUBLE = 6
        }

        [Serializable]
        public class BoolCallback : SerializableCallback<bool>
        {
        }

        [Serializable]
        public class IntCallback : SerializableCallback<int>
        {
        }

        [Serializable]
        public class StringCallback : SerializableCallback<string>
        {
        }

        [Serializable]
        public class FloatCallback : SerializableCallback<float>
        {
        }

    }
}