using UnityEditor;
using com.FDT.GameEvents.Editor;

namespace com.FDT.Analytics.Editor
{
	[CustomEditor(typeof(AnalyticsGameEvent))]
	public class AnalyticsGameEventEditor : GameEvent1Editor<AnalyticEventAsset>
	{
	}
}
