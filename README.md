# FDT Analytics

Generic analytics system.



## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.analytics": "https://bitbucket.org/fdtdev/fdtanalytics.git#1.0.0",
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.0.0",
	"com.fdt.gameevents": "https://bitbucket.org/fdtdev/fdtgameevents.git#2.0.0",
	"com.github.fdtdev.serializablecallback": "https://github.com/fdtdev/SerializableCallback.git"

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtanalytics/src/1.0.0/LICENSE.md)